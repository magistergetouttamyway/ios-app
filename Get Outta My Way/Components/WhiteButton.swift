//
//  WhiteButton.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 04/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import UIKit

class WhiteButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        self.layer.cornerRadius = 5
        self.setTitleColor(UIColor.fromRGB(rgbValue: 0x039ed7), for: .normal)
    }
    
    
}
