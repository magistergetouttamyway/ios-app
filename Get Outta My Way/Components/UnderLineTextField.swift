//
//  UnderLineTextField.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 04/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import UIKit

class UnderLineTextField : UITextField {
    
    override var tintColor: UIColor! {
        
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 1.0
        
        tintColor.setStroke()
        
        path.stroke()
    }
}
