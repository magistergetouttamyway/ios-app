//
//  ViewController.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 14/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import UIKit
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import Mapbox
import Turf

class ViewController: UIViewController {
    let authService = AuthService()
    
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var errorMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.authService.delegate = self
        self.setupViews()
        self.hideKeyboardOnTap()
        self.useGradientBackground()
    }
    
    func setupViews() {
        self.errorMessageLabel.text = ""
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        self.authService.login(username: self.usernameTextField.text!, password: self.passwordTextField.text!)
    }
    
    @IBAction func unwindToLoginView(_ unwindSegue: UIStoryboardSegue) {
        self.errorMessageLabel.text = ""
    }
    
    @IBAction func unwindToLoginViewRegisterSuccessful(_ unwindSegue: UIStoryboardSegue) {
        self.errorMessageLabel.text = "Registering successful!"
    }
}

extension ViewController: AuthServiceDelegate {
    func loginSuccessful() {
        self.errorMessageLabel.text = ""
        self.performSegue(withIdentifier: "ShowMapViewController", sender: nil)
    }
    
    func loginFailed() {
        self.errorMessageLabel.text = "Invalid credentials"
    }
}
