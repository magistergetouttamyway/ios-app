//
//  MapViewController.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 24/04/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import UIKit
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections
import Mapbox
import Turf

let coordinatesOfEeden: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 58.377455046022085, longitude: 26.747851967811584)
let coordinatesOfLounakeskus: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 58.35887242214752, longitude: 26.67531967163086)

class MapViewController: UIViewController {
    var mapView: NavigationMapView!
    var currentRoute: Route? {
        get {
            return routes?.first
        }
        set {
            guard let selected = newValue else { routes?.remove(at: 0); return }
            guard let routes = routes else { self.routes = [selected]; return }
            self.routes = [selected] + routes.filter { $0 != selected }
        }
    }
    var routes: [Route]? {
        didSet {
            guard let routes = routes, let current = routes.first else { mapView?.removeRoutes(); return }
            mapView?.showRoutes(routes)
            mapView?.showWaypoints(current)
        }
    }
    
    var startButton: UIButton?
    var locationManager = CLLocationManager()
    
    private typealias RouteRequestSuccess = (([Route]) -> Void)
    private typealias RouteRequestFailure = ((NSError) -> Void)

    var emergencyVehicleService = EmergencyVehicleService()
    let socketService = SocketService()
    let mapService = MapService()
    let navigationMapService = MapService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView = NavigationMapView(frame: view.bounds)
        mapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView?.userTrackingMode = .follow
        mapView?.delegate = self
        mapView?.navigationMapDelegate = self
        
        mapService.mapView = self.mapView
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        mapView?.addGestureRecognizer(gesture)
        
        view.addSubview(mapView!)
        
        startButton = WhiteButton()
        startButton?.setTitle("Start Navigation", for: .normal)
        startButton?.translatesAutoresizingMaskIntoConstraints = false
        startButton?.contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        startButton?.addTarget(self, action: #selector(tappedButton(sender:)), for: .touchUpInside)
        startButton?.isHidden = true
        view.addSubview(startButton!)
        startButton?.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        startButton?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        view.setNeedsLayout()
        
        self.setupSocket()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startButton?.layer.cornerRadius = startButton!.bounds.midY
        startButton?.clipsToBounds = true
        startButton?.setNeedsDisplay()
    }
    
    @objc func tappedButton(sender: UIButton) {
        guard let route = currentRoute else { return }
        let navigationService = MapboxNavigationService(route: route, simulating: .never)
        let navigationOptions = NavigationOptions(navigationService: navigationService)
        let navigationViewController = NavigationViewController(for: route, options: navigationOptions) as NavigationViewController
        navigationViewController.delegate = self
        navigationViewController.mapView?.delegate = self
        
        
        self.navigationMapService.mapView = navigationViewController.mapView
        self.navigationMapService.userRouteCoordinates = navigationViewController.route.coordinates!
        
        present(navigationViewController, animated: true, completion: nil)
    }
    
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .ended else { return }
        
        let spot = gesture.location(in: mapView)
        guard let location = mapView?.convert(spot, toCoordinateFrom: mapView) else { return }
        
        requestRoute(destination: location)
    }
    
    func requestRoute(destination: CLLocationCoordinate2D) {
        guard let userLocation = mapView?.userLocation!.location else { return }
        let userWaypoint = Waypoint(location: userLocation, heading: mapView?.userLocation?.heading, name: "user")
        let destinationWaypoint = Waypoint(coordinate: destination)
        
        let options = NavigationRouteOptions(waypoints: [userWaypoint, destinationWaypoint])
        
        Directions.shared.calculate(options) { (waypoints, routes, error) in
            guard let routes = routes else { return }
            self.routes = routes
            self.startButton?.isHidden = false
            self.mapView?.showRoutes(routes)
            self.mapView?.showWaypoints(self.currentRoute!)
        }
    }
    
    func navigationMapView(_ mapView: NavigationMapView, didSelect route: Route) {
        self.currentRoute = route
    }
    
    func setupSocket() {
        socketService.delegate = self
        socketService.connect()
    }
}

extension MapViewController: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if let vehicleAnnotation = annotation as? VehicleAnnotation {
            return AnnotationView(annotation: vehicleAnnotation)
        }
        
        if let intersectionAnnotation = annotation as? IntersectionAnnotation {
            return AnnotationView(annotation: intersectionAnnotation)
        }
        return nil
    }
}

extension MapViewController: EmergencyVehicleDelegate {
    func emergencyVehicleRouteNearMe(emergencyVehicle: EmergencyVehicle) {
        mapService.addEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
        navigationMapService.addEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
    }
    
    func emergencyVehicleDeactivated(emergencyVehicle: EmergencyVehicle) {
        mapService.removeEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
        navigationMapService.removeEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
    }
    
    func emergencyVehicleOutOfRange(emergencyVehicle: EmergencyVehicle) {
        mapService.removeEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
        navigationMapService.removeEmergencyVehicleMapObject(object: EmergencyVehicleMapObject(emergencyVehicle: emergencyVehicle))
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
}

extension MapViewController: NavigationMapViewDelegate {
    func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
        if let location = userLocation?.location {
            socketService.updateCurrentLocation(location: location)
        }
    }
}

extension MapViewController: NavigationViewControllerDelegate {
    func navigationViewControllerDidDismiss(_ navigationViewController: NavigationViewController, byCanceling canceled: Bool) {
        self.navigationMapService.mapView = nil
        self.navigationMapService.userRouteCoordinates = []
        navigationViewController.dismiss(animated: true, completion: nil)
    }
}
