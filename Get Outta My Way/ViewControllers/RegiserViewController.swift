//
//  RegiserViewController.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 03/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var firsntnameTextField: UITextField!
    @IBOutlet var lastnameTextField: UITextField!
    @IBOutlet var errorMessageLabel: UILabel!
    
    let authService = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardOnTap()
        self.useGradientBackground()
        authService.registerDelegate = self
        errorMessageLabel.text = ""
    }
    
    @IBAction func registerButtonTapped(_ sender: Any) {
        if
            usernameTextField.text!.isEmpty ||
            passwordTextField.text!.isEmpty ||
            emailTextField.text!.isEmpty ||
            firsntnameTextField.text!.isEmpty ||
            lastnameTextField.text!.isEmpty {
            errorMessageLabel.text = "All fields are required!"
        } else {
            let user = User(email: emailTextField.text!, username: usernameTextField.text!, password: passwordTextField.text!, firstname: firsntnameTextField.text!, lastname: lastnameTextField.text!)
            authService.register(user: user)
            errorMessageLabel.text = ""
        }
        
    }
}


extension RegisterViewController: RegisterUserDelegate {
    func registerFailed(error: CommonError) {
        errorMessageLabel.text = error.message
    }
    
    func registerSuccessful() {
        performSegue(withIdentifier: "unwindAfterRegisterSuccess", sender: nil)
    }
}
