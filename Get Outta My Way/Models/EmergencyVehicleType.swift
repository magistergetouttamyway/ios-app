//
//  EmergencyVehicleType.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation

enum EmergencyVehicleType: String {
    case Ambulance
    case Police
    case FireTruck
}
