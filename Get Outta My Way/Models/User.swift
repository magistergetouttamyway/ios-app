//
//  User.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 03/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation


struct User {
    let email: String
    let username: String
    let password: String
    let firstname: String
    let lastname: String
}
