//
//  EmergencyVehicle.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox
import SwiftyJSON

struct EmergencyVehicle {
    let id: String
    let name: String
    let type: EmergencyVehicleType
    let location: EmergencyVehicleLocation
    var intersections: [MGLPointAnnotation]
    var overlappings: [MGLPolyline]
}

extension EmergencyVehicle {
    init?(json: JSON) {
        guard
            let id = json["id"].string,
            let name = json["name"].string,
            let type = json["type"].string,
            let location = EmergencyVehicleLocation(json: json["location"])
            else {
                return nil
        }
    
        self.id = id
        self.name = name
        self.type = EmergencyVehicleType(rawValue: type) ?? EmergencyVehicleType.Ambulance
        self.intersections = []
        self.overlappings = []
        self.location = location
    }
    
    mutating func setIntersection(json: JSON) {
        guard let intersectionsJsonArray = json["intersections"].array else {
            return
        }
        let intersections = intersectionsJsonArray.compactMap { (intersectionJson) -> MGLPointAnnotation? in
            guard let intersection = try? MGLShape(data: intersectionJson.rawData(), encoding: UTF8Encoding) else {
                return nil
            }
            return intersection as? MGLPointAnnotation
        }
        self.intersections = intersections
    }
    
    mutating func setOverlapoings(json: JSON) {
        guard let overlapsJsonArray = json["overlappings"].array else {
            return
        }
        let overlappings = overlapsJsonArray.compactMap { (overlappingJson) -> MGLPolyline? in
            guard let overlapping = try? MGLShape(data: overlappingJson.rawData(), encoding: UTF8Encoding) else {
                return nil
            }
            return overlapping as? MGLPolyline
        }
        self.overlappings = overlappings
    }
    
    func getCurrentLocationAnnotation() -> VehicleAnnotation {
        return VehicleAnnotation(coordinate: self.location.currentLocation.coordinate, vehicleType: self.type)
    }
    
    func getIntersectionAnnotations() -> [IntersectionAnnotation] {
        return self.intersections.map({ (annotation) -> IntersectionAnnotation in
            return IntersectionAnnotation(coordinate: annotation.coordinate)
        })
    }
}
