//
//  Error.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 03/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CommonError {
    let message: String;
    let name: String
    
    init?(json: JSON) {
        guard
            let message = json["message"].string,
            let name = json["name"].string
            else {
                return nil
        }
        
        self.message = message
        self.name = name
    }
    
    init(name: String, message: String) {
        self.message = message
        self.name = name
    }
}
