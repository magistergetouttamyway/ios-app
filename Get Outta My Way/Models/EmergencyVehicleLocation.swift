//
//  EmergencyVehicleLocation.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox
import SwiftyJSON
import Turf

struct EmergencyVehicleLocation {
    let id: String
    let currentLocation: MGLPointAnnotation
    let route: MGLPolyline
}

extension EmergencyVehicleLocation {
    init?(json: JSON) {
        // print(json["route"])
        guard
            let id = json["id"].string,
            let currentLocation = try? MGLShape(data: json["currentLocation"].rawData(), encoding: UTF8Encoding),
            let route = try? MGLShape(data: json["route"].rawData(), encoding: UTF8Encoding)
            else {
                return nil
        }
    
        self.currentLocation = currentLocation as! MGLPointAnnotation
        self.route = route as! MGLPolyline
        self.id = id
    }
}
