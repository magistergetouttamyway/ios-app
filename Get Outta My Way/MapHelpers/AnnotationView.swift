//
//  AnnotationView.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox

class AnnotationView: MGLAnnotationView {
    let defaultBounds = CGRect(x:0, y:0, width: 40, height: 40)
    
    init(annotation: VehicleAnnotation) {
        super.init(reuseIdentifier: annotation.reuseIdentifier)
        setup(image: annotation.image)
    }
    
    init(annotation: IntersectionAnnotation) {
        super.init(reuseIdentifier: annotation.reuseIdentifier)
        setup(image: annotation.image)
    }
    
    func setup(image: UIImage) {
        self.bounds = defaultBounds
        let imageView = UIImageView(frame: defaultBounds)
        imageView.image = image
        //imageView.tintColor = .orange
        self.addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
