//
//  IntersectionAnnotation.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox

class IntersectionAnnotation: Annotation {
    var reuseIdentifier: String
    var image: UIImage
    
    init(coordinate: CLLocationCoordinate2D) {
        self.reuseIdentifier = "IntersectionAnnotation"
        self.image = UIImage(named: "Intersection")!
        super.init(coordinate: coordinate)
    }
    
    var annotationImage: MGLAnnotationImage {
        get {
            return MGLAnnotationImage(image: self.image, reuseIdentifier: self.reuseIdentifier)
        }
    }
}
