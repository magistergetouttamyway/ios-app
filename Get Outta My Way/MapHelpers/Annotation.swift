//
//  Annotation.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox

class Annotation: NSObject, MGLAnnotation {
    // MGLAnnotation
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String? = nil, subtitle: String? = nil) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
