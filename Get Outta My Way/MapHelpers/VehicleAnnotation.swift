//
//  VehicleAnnotation.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 20/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox

class VehicleAnnotation: Annotation {
    var reuseIdentifier: String
    var image: UIImage
    
    init(coordinate: CLLocationCoordinate2D, vehicleType: EmergencyVehicleType) {
        self.reuseIdentifier = "\(vehicleType.rawValue)Annotation"
        self.image = UIImage(named: vehicleType.rawValue)!
        super.init(coordinate: coordinate, title: vehicleType.rawValue)
    }
    
    var annotationImage: MGLAnnotationImage {
        get {
            return MGLAnnotationImage(image: self.image, reuseIdentifier: self.reuseIdentifier)
        }
    }
}
