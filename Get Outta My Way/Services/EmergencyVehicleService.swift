//
//  EmergencyVehicleService.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 18/02/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Alamofire
import Mapbox
import SwiftyJSON
import Turf

class EmergencyVehicleService {
    func findIntersectionsAndOverlappings(emergencyVehicle: EmergencyVehicle, userRoute: MGLPolyline, callback: @escaping ((EmergencyVehicle) -> Void)) {
        let emergencyVehicleRoute = try? JSONSerialization.jsonObject(with: emergencyVehicle.location.route.geoJSONData(usingEncoding: String.Encoding.utf8.rawValue), options: []) as? [String: Any]
        let userRoute = try? JSONSerialization.jsonObject(with: userRoute.geoJSONData(usingEncoding: String.Encoding.utf8.rawValue), options: []) as? [String: Any]
        
        let params: Parameters = [
            "route1": emergencyVehicleRoute as Any,
            "route2": userRoute as Any
        ]
        
        AF.request("\(Config.serverUrl)/api/map-utils/routes-intersect", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                let responseJSON = JSON(response.result.value as Any)
                var responseVehicle = emergencyVehicle
                responseVehicle.setIntersection(json: responseJSON)
                responseVehicle.setOverlapoings(json: responseJSON)
                callback(responseVehicle)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func emergencyVehicleIsOutOfRange(emergencyVehicle: EmergencyVehicle, userLocation: CLLocation) -> Bool {
        let c1 = emergencyVehicle.location.currentLocation.coordinate
        let c2 = userLocation.coordinate
        
        let distance = c1.distance(to: c2)
        return distance > 5000
    }
}
