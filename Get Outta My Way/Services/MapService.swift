//
//  MapService.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 11/04/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Mapbox

class EmergencyVehicleMapObject {
    var emergencyVehicle: EmergencyVehicle
    var vehicleAnnotation: VehicleAnnotation? = nil
    var vehicleIntersectionAnnotations: [IntersectionAnnotation] = []
    var overlappings: [MGLPolyline] = []
    init(emergencyVehicle: EmergencyVehicle) {
        self.emergencyVehicle = emergencyVehicle
    }
}

class MapService {
    var mapView: MGLMapView?
    var userRouteCoordinates: [CLLocationCoordinate2D] = []
    
    private var emergencyVehicleMapObjects: [EmergencyVehicleMapObject] = []
    private var emergencyVehicleService = EmergencyVehicleService()
    
    func addEmergencyVehicleMapObject(object: EmergencyVehicleMapObject) {
        let (existingObject, _) = findMapObject(object: object)
        
        if let existingObject = existingObject {
            existingObject.emergencyVehicle = object.emergencyVehicle
        } else {
            emergencyVehicleMapObjects.append(object)
        }
        
        let (addedObject, _) = findMapObject(object: object)

        if let addedObject = addedObject {
            drawPolyLine(vehicleObject: addedObject)
            drawVehicleLocation(vehicleObject: addedObject)
            drawIntersectionsAndOverlappings(vehicleObject: addedObject)
        }
    }
    
    func removeEmergencyVehicleMapObject(object: EmergencyVehicleMapObject) {
        let (existingObject, index) = findMapObject(object: object)

        if let existingObject = existingObject {
            cleanup(vehicleObject: existingObject)
        }
        
        if let index = index {
            emergencyVehicleMapObjects.remove(at: index)
        }
    }
    
    private func emergencyVehicleObjectAlreadyExists(object: EmergencyVehicleMapObject) -> Bool {
        return emergencyVehicleMapObjects.contains(where: { (emergencyVehicleObject) -> Bool in
            return emergencyVehicleObject.emergencyVehicle.id == object.emergencyVehicle.id
        })
    }
    
    private func findMapObject(object: EmergencyVehicleMapObject) -> (object: EmergencyVehicleMapObject?, index: Int?) {
        let index = emergencyVehicleMapObjects.firstIndex { (emergencyVehicleObject) -> Bool in
            return emergencyVehicleObject.emergencyVehicle.id == object.emergencyVehicle.id
        }
        let object = emergencyVehicleMapObjects.first { (emergencyVehicleObject) -> Bool in
            return emergencyVehicleObject.emergencyVehicle.id == object.emergencyVehicle.id
        }
        return (object, index)
    }
    
    private func drawPolyLine(vehicleObject: EmergencyVehicleMapObject) {
        if let source = updateSource(vehicleObject: vehicleObject, shape: vehicleObject.emergencyVehicle.location.route, id: vehicleObject.emergencyVehicle.id) {
            addPolylineLayer(vehicleObject: vehicleObject, source: source, id: vehicleObject.emergencyVehicle.id)
        }
    }
    
    private func updateSource(vehicleObject: EmergencyVehicleMapObject, shape: MGLShape, id: String) -> MGLShapeSource? {
        guard let style = self.mapView?.style else { return nil}
        
        if let existingSource = style.source(withIdentifier: id) as? MGLShapeSource {
            existingSource.shape = shape
            return existingSource
        }
        
        let newSource = MGLShapeSource(identifier: id, shape: shape, options: nil)
        style.addSource(newSource)
        return newSource
    }
    
    private func drawOverlapping(vehicleObject: EmergencyVehicleMapObject) {
        guard vehicleObject.overlappings.count > 0 else { return }
        
        for overlapping in vehicleObject.overlappings {
            let id =  "\(vehicleObject.emergencyVehicle.id)-overlapping-\(overlapping.coordinate.latitude)-\(overlapping.coordinate.longitude)"
            if let source = updateSource(vehicleObject: vehicleObject, shape: overlapping, id: id){
                addPolylineLayer(vehicleObject: vehicleObject, source: source, id: id, lineColor: .red)
            }
        }
    }
    
    private func addPolylineLayer(vehicleObject: EmergencyVehicleMapObject, source: MGLShapeSource, id: String, lineColor: UIColor = .orange) {
        guard let style = self.mapView?.style else { return }
        
        if style.layer(withIdentifier: id) == nil {
            let layer = MGLLineStyleLayer(identifier: id, source: source)
            layer.lineJoin = NSExpression(forConstantValue: "round")
            layer.lineCap = NSExpression(forConstantValue: "round")
            layer.lineColor = NSExpression(forConstantValue: lineColor)
            // Use `NSExpression` to smoothly adjust the line width from 2pt to 20pt between zoom levels 14 and 18. The `interpolationBase` parameter allows the values to interpolate along an exponential curve.
            layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                           [14: 10, 18: 20])
            style.addLayer(layer)
        }
    }
    
    private func drawVehicleLocation(vehicleObject: EmergencyVehicleMapObject) {
        if let vehicleAnnotation = vehicleObject.vehicleAnnotation {
            self.mapView?.removeAnnotation(vehicleAnnotation)
        }
        vehicleObject.vehicleAnnotation = vehicleObject.emergencyVehicle.getCurrentLocationAnnotation()
        
        self.mapView?.addAnnotation(vehicleObject.vehicleAnnotation!)
    }
    
    private func drawIntersectionsAndOverlappings(vehicleObject: EmergencyVehicleMapObject) {
        guard self.userRouteCoordinates.count > 0 else { return }
        let userRoute = MGLPolyline(coordinates: self.userRouteCoordinates, count: UInt(self.userRouteCoordinates.count))
        emergencyVehicleService.findIntersectionsAndOverlappings(emergencyVehicle: vehicleObject.emergencyVehicle, userRoute: userRoute) { (vehicle) in
            self.mapView?.removeAnnotations(vehicleObject.vehicleIntersectionAnnotations)
            self.removeOverlappings(vehicleObject: vehicleObject)
            vehicleObject.vehicleIntersectionAnnotations = vehicle.getIntersectionAnnotations()
            vehicleObject.overlappings = vehicle.overlappings
            self.mapView?.addAnnotations(vehicleObject.vehicleIntersectionAnnotations)
            self.drawOverlapping(vehicleObject: vehicleObject)
        }
    }
    
    private func cleanup(vehicleObject: EmergencyVehicleMapObject) {
        removePolyline(vehicleObject: vehicleObject)
        removeVehicleLocation(vehicleObject: vehicleObject)
        removeIntersections(vehicleObject: vehicleObject)
        removeOverlappings(vehicleObject: vehicleObject)
    }
    
    private func removePolyline(vehicleObject: EmergencyVehicleMapObject) {
        guard let style = self.mapView?.style else { return }
        if let vehicleLayer = style.layer(withIdentifier: vehicleObject.emergencyVehicle.id) {
            style.removeLayer(vehicleLayer)
        }
        
        if let vehicleSource = style.source(withIdentifier: vehicleObject.emergencyVehicle.id) {
            style.removeSource(vehicleSource)
        }
    }
    
    private func removeVehicleLocation(vehicleObject: EmergencyVehicleMapObject) {
        if let vehicleAnnotation = vehicleObject.vehicleAnnotation {
            self.mapView?.removeAnnotation(vehicleAnnotation)
        }
    }
    
    private func removeIntersections(vehicleObject: EmergencyVehicleMapObject) {
        self.mapView?.removeAnnotations(vehicleObject.vehicleIntersectionAnnotations)
    }
    
    private func removeOverlappings(vehicleObject: EmergencyVehicleMapObject) {
        guard let style = self.mapView?.style else { return }
        
        for overlapping in vehicleObject.overlappings {
            let id =  "\(vehicleObject.emergencyVehicle.id)-overlapping-\(overlapping.coordinate.latitude)-\(overlapping.coordinate.longitude)"
            
            if let overlappingLayer = style.layer(withIdentifier: id) {
                style.removeLayer(overlappingLayer)
            }
            
            if let overlappingSource = style.source(withIdentifier: id) {
                style.removeSource(overlappingSource)
            }
        }
    }
}
