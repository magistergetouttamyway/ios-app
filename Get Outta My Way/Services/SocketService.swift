//
//  UserService.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 13/03/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import SocketIO
import Mapbox
import SwiftyJSON

struct AuthenticationData: SocketData {
    let token: String
    let emitEvent = SocketEmitEvent.authenticate
    
    func socketRepresentation() -> SocketData {
        return ["token": token]
    }
}

struct LocationData: SocketData {
    let currentLocation: CLLocationCoordinate2D
    let emitEvent = SocketEmitEvent.locationUpdate
    
    func socketRepresentation() -> SocketData {
        let point = MGLPointAnnotation()
        point.coordinate = currentLocation
        
        let json = try? JSONSerialization.jsonObject(with: point.geoJSONData(usingEncoding: String.Encoding.utf8.rawValue), options: []) as? [String: Any]
        return ["currentLocation": json]
    }
}

struct LeaveRoomData: SocketData {
    let roomId: String
    let emitEvent = SocketEmitEvent.leaveRoom
    
    func socketRepresentation() -> SocketData {
        return ["roomId": roomId]
    }
}

enum SocketReceivedEvent: String {
    case authenticated
    case unauthorized
}

enum SocketEmitEvent: String {
    case authenticate
    case locationUpdate = "client-location-update"
    case leaveRoom = "client-leave-room"
}

protocol EmergencyVehicleDelegate: class {
    func emergencyVehicleRouteNearMe(emergencyVehicle: EmergencyVehicle)
    func emergencyVehicleDeactivated(emergencyVehicle: EmergencyVehicle)
    func emergencyVehicleOutOfRange(emergencyVehicle: EmergencyVehicle)
}

class SocketService: NSObject {
    weak var delegate: EmergencyVehicleDelegate?

    private var manager: SocketManager = SocketManager(socketURL: URL(string: Config.serverUrl)!, config: [.log(false), .secure(false), .reconnects(true), .forceNew(true)])
    private var socket: SocketIOClient!
    private var eventHandlers: [String] = []
    private let authService = AuthService()
    
    var userCurrentLocation: CLLocation?
    
    override init() {
        socket = manager.defaultSocket
    }
    
    func connect() {
        guard let token = authService.token else {
            return
        }
        
        socket!.on(clientEvent: .connect) { data, ack in
            let authData = AuthenticationData(token: token)
            self.socket.emit(authData.emitEvent.rawValue, authData)
        }
        
        socket.on("authenticated") { data, ack in
            print("authenticated")
            
            self.socket.on("emergency-vehicle-nearby") {data, ack in
                if let emergencyVehicle = self.emergencyVehicleFrom(data: data) {
                    if let userCurrentLocation = self.userCurrentLocation {
                        if EmergencyVehicleService().emergencyVehicleIsOutOfRange(emergencyVehicle: emergencyVehicle, userLocation: userCurrentLocation) {
                            let leaveRoomData = LeaveRoomData(roomId: emergencyVehicle.id)
                            self.socket.emit(leaveRoomData.emitEvent.rawValue, leaveRoomData)
                            self.delegate?.emergencyVehicleOutOfRange(emergencyVehicle: emergencyVehicle)
                            return
                        }
                    }
                    self.delegate?.emergencyVehicleRouteNearMe(emergencyVehicle: emergencyVehicle)
                }
            }
            
            self.socket.on("emergency-vehicle-deactivated") {data, ack in
                if let emergencyVehicle = self.emergencyVehicleFrom(data: data) {
                    self.delegate?.emergencyVehicleDeactivated(emergencyVehicle: emergencyVehicle)
                }
            }
        }
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print("Disonnect")
            self.socket.off("emergency-vehicle-nearby")
            self.socket.off("emergency-vehicle-deactivated")
        }
        
        socket.connect()
    }
    
    func disconnect() {
        socket.disconnect()
    }
    
    func updateCurrentLocation(location: CLLocation) {
        self.userCurrentLocation = location
        let locationData = LocationData(currentLocation: location.coordinate)
        socket.emit(locationData.emitEvent.rawValue, locationData)
    }
    
    private func emergencyVehicleFrom(data: [Any]) -> EmergencyVehicle? {
        guard let dict = data[0] as? [String: Any] else {
            return nil
        }
        
        if let json = try? JSONSerialization.data(withJSONObject: dict, options: []) {
            if let x = try? JSON(data: json) {
                if let emergencyVehicle = EmergencyVehicle(json: x) {
                    return emergencyVehicle
                }
            }
        }
        return nil
    }
}
