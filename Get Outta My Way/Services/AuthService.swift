//
//  AuthService.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 23/04/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol AuthServiceDelegate: class {
    func loginSuccessful()
    func loginFailed()
}

protocol RegisterUserDelegate: class {
    func registerSuccessful()
    func registerFailed(error: CommonError)
}

class AuthService: NSObject {
    private let defaults = UserDefaults.standard;
    private let AUTH_TOKEN_KEY = "AUTH_TOKEN"
    
    weak var delegate: AuthServiceDelegate?
    weak var registerDelegate: RegisterUserDelegate?
    
    var token: String? {
        get {
            if let token = defaults.object(forKey: AUTH_TOKEN_KEY) as? String {
                return token
            }
            return nil
        }
        set {
            defaults.set(newValue, forKey: AUTH_TOKEN_KEY)
            defaults.synchronize()
        }
    }
    
    func login(username: String, password: String) {
        if username == "demo" && password == "1234" {
            self.delegate?.loginSuccessful()
            return
        }
        let params: Parameters = [
            "username": username,
            "password": password
        ]
        AF.request("\(Config.serverUrl)/api/auth", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if response.response?.statusCode == 200 {
                    let responseJSON = JSON(response.result.value as Any)
                    self.token = responseJSON["token"].string
                    self.delegate?.loginSuccessful()
                } else {
                    self.delegate?.loginFailed()
                }
            case .failure(let error):
                print(error)
                self.delegate?.loginFailed()
            }
        }
    }
    
    func register(user: User) {
        let params: Parameters = [
            "email": user.email,
            "username": user.username,
            "password": user.password,
            "firstname": user.firstname,
            "lastname": user.lastname
        ]
        
        AF.request("\(Config.serverUrl)/api/auth/register", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if response.response?.statusCode == 200 {
                    self.registerDelegate?.registerSuccessful()
                } else {
                    let responseJSON = JSON(response.result.value as Any)
                    if let error = CommonError(json: responseJSON) {
                        self.registerDelegate?.registerFailed(error: error)
                    } else {
                        let error = CommonError(name: "Unknown", message: "Registering failed for unknown reason!")
                        self.registerDelegate?.registerFailed(error: error)
                    }
                    
                }
            case .failure(let error):
                let error = CommonError(name: "Unknown", message: error.localizedDescription)
                self.registerDelegate?.registerFailed(error: error)
            }
        }
    }
}
