//
//  UIVIewControllerExtension.swift
//  Get Outta My Way
//
//  Created by Vello Vaherpuu on 03/05/2019.
//  Copyright © 2019 Vello Vaherpuu. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func useGradientBackground() {
        let gradient: CAGradientLayer = CAGradientLayer()
        let startingColor = UIColor.fromRGB(rgbValue: 0x039ed7)
        let endColor = UIColor.fromRGB(rgbValue: 0x734FD0)
        gradient.colors = [startingColor.cgColor, endColor.cgColor]
        gradient.locations = [0.3 , 1.0]
        gradient.startPoint = CGPoint(x: 0.3, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.6, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
}
