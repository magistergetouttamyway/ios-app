## ❯ Getting Started

### Step 1: Set up the Development Environment

Requirements:
* Mac OS
* xCode 10+

You need to set up your development environment before you can do anything.

Install [CocoaPods](https://cocoapods.org/)

Move to project directory

Install dependencies

```bash
pod install
```

After installation make sure that the project is opened in xcode by opening the file 'Get Outta My Way.xcworkspace' not 'Get Outta My Way.xcodeproj'.

### Step 2: Change server url

You need to change the server url in the config file which is located in Get\ Outta\ My\ Way/Config/Config.swift.

When running the server locally the url should be similar to: http://ip-of-your-computer:3000


### Step 3: Run project

Run the project on any iPhone simulator or real device.
